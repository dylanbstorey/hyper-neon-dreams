// CONSTANTS

const PINK = 'rgb(250, 25, 153)';
const TEAL = 'rgb(97,226,254)';
const GOLD = 'rgba(254, 203, 0, 1)';
const PURPLE = 'rgb(88, 0, 226)';
const DEEP_PURPLE = 'rgb(32,9,51)'; 
const LIGHT_PURPLE = 'rgb(67, 9, 73)';
const WHITE = 'rgb(100,100,100)';
const TRANSPARENT = 'rgba(32,9,51,0)';



exports.decorateConfig = (config) => {
    return Object.assign({}, config, {
      cursorColor: GOLD,
      cursorBlink: true,
      backgroundColor : DEEP_PURPLE,
      foregroundColor : TEAL,
      borderColor : DEEP_PURPLE,
      fontSize: 13,
      termCSS: `
			${config.termCSS || ''}
			* {
				text-shadow: 0px 0px 10px ${TEAL}, 0px 0px 20px ${TEAL} ;
			}
		`,
      css: `
        ${config.css || ''}
        .terms_terms {
            background-color: transparent !important;
            background-image: linear-gradient(to bottom, ${DEEP_PURPLE} 70%, ${LIGHT_PURPLE});
            background-size: cover;
            text-shadow: 0px 0px 10px ${TEAL}, 0px 0px 20px ${TEAL} ;
          }
        .terms_terms:after{
            content:'';
            height:300px;
            width:100%;
            display:block;
            background-image:linear-gradient(90deg, rgba(252,25,154,.1) 1px, rgba(0,0,0,0) 1px), linear-gradient(0deg, rgba(252,25,154,.1) 1px, rgba(0,0,0,0) 1px);
            background-position:bottom;
            background-repeat:repeat;
            background-size:20px 20px;
            left: -25px;
            position: absolute;
            pointer-events: none;
            bottom: 0;
            transform: perspective(100px) rotateX(60deg);
            z-index: 0;
        }
        .cursor-node {
            background-color: ${GOLD} !important;
            border-color: ${GOLD} !important;
          }
        .tabs_nav .tabs_list .tab_text{
          color: ${GOLD};
          background: ${DEEP_PURPLE}
          border: ${DEEP_PURPLE}
          text-shadow: 0px 0px 10px ${GOLD};
        }

        .header_header {
            background: ${DEEP_PURPLE} !important;
          }
        .tab_textActive {
            background:
            linear-gradient(to bottom, ${LIGHT_PURPLE}, ${DEEP_PURPLE})
            no-repeat !important; 
            border: 0 !important;
            border-image: none !important;
            text-shadow: 0px 0px 10px ${GOLD}, 0px 0px 20px ${GOLD} ;
          }
      `
    });
  }
