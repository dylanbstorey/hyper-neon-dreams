# hyper-neon-dreams

![](screenshot.png)

## Install

Add `hyper-neon-dreams` to the plugins list in `~/.hyper.js`

```
plugins: [
    'hyper-neon-dreams,
    ...
]
```